package com.tanisorn.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Tanisorn", 100);
        book.withdraw(50);
        assertEquals(50, book.getBalance(), 0.00001);   
    }


    @Test
    public void shouldWithdrawOverBalance() {
        BookBank book = new BookBank("Tanisorn", 100);
        book.withdraw(150);
        assertEquals(100, book.getBalance(), 0.00001);  
    }



    @Test
    public void shouldWithdrawWithNegativeNumber() {
        BookBank book = new BookBank("Tanisorn", 100);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(), 0.00001); 
    }



    @Test
    public void shoulddepositSuccess() {
        BookBank book = new BookBank("Tanisorn", 100);
        book.deposit(50);
        assertEquals(150, book.getBalance(), 0.00001);   
    }


    
    @Test
    public void shoulddepositOverBalance() {
        BookBank book = new BookBank("Tanisorn", 100);
        book.deposit(150);
        assertEquals(100, book.getBalance(), 0.00001);   
    }
}
