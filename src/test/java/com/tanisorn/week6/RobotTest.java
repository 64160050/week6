package com.tanisorn.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }


    @Test
    public void shouldDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down(5));
        assertEquals(5, robot.getY());
    }

    @Test
    public void shouldDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 10);
        assertEquals(true, robot.down(5));
        assertEquals(15, robot.getY());
    }



    @Test
    public void shouldDownNfail1() {
        Robot robot = new Robot("Robot", 'R', 0, 5);
        assertEquals(false, robot.down(18));
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldDownNfail2() {
        Robot robot = new Robot("Robot", 'R', 0, 10);
        assertEquals(false, robot.down(11));
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY());
    }


    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up(11));
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(false, robot.up(12));
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldLeftNegative() {
        Robot robot = new Robot("Robot", 'R', Robot.MIN_X, 0);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }

    @Test
    public void shouldLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(false, robot.left());
        assertEquals(0, robot.getX());
    }


    @Test
    public void shouldLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 3, 0);
        assertEquals(true, robot.left(2));
        assertEquals(1, robot.getX());
    }



    @Test
    public void shouldLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 0);
        assertEquals(true, robot.left(5));
        assertEquals(5, robot.getX());
    }


    @Test
    public void shouldLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 0);
        assertEquals(false, robot.left(11));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldLeftNFail2() {
        Robot robot = new Robot("Robot", 'R', 9, 0);
        assertEquals(false, robot.left(13));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRightOver() {
        Robot robot = new Robot("Robot", 'R', Robot.MAX_X, 0);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }

    @Test
    public void shouldRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 18, 0);
        assertEquals(true, robot.right());
        assertEquals(19, robot.getX());
    }


    @Test
    public void shouldRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 18, 0);
        assertEquals(true, robot.right(1));
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 0);
        assertEquals(true, robot.right(6));
        assertEquals(16, robot.getX());
    }


    @Test
    public void shouldRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 14, 0);
        assertEquals(false, robot.right(8));
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRightNFail2() {
        Robot robot = new Robot("Robot", 'R', 9, 0);
        assertEquals(false, robot.right(15));
        assertEquals(19, robot.getX());
    }
}
