package com.tanisorn.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank tanisorn = new BookBank("Tanisorn", 50.0);  // Constructor
        
        tanisorn.print();
        BookBank prayud = new BookBank("PraYud", 100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        tanisorn.deposit(40000.0);
        tanisorn.print();

        BookBank prawit = new BookBank("Prawit", 1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
